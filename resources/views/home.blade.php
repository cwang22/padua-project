<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Padua project</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
<div class="container">

    @error('csv')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror

    <form action="{{ url('/') }}" method="post" enctype="multipart/form-data" class="mb-2">
        {{ csrf_field() }}
        <h2>Upload new CSV</h2>
        <label for="csv" class="d-block">Select CSV to upload</label>
        <input type="file" name="csv" id="csv" class="d-block">
        <input class="mt-2" type="submit" value="Upload CSV">
    </form>

    @if(count($transactions) > 0)
        <h2>Bank transactions from CSV</h2>
        <table class="table">
            <thead>
            <tr>
                <th>Date</th>
                <th>Transaction Code</th>
                <th>Valid Transaction?</th>
                <th>Customer Number</th>
                <th>Reference</th>
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>
            @foreach($transactions as $transaction)
                <tr>
                    <td>{{ $transaction->getFormattedDate() }}</td>
                    <td>{{ $transaction->transactionCode }}</td>
                    <td>{{ $transaction->isValid() ? 'Yes' : 'No' }}</td>
                    <td>{{ $transaction->customerNumber }}</td>
                    <td>{{ $transaction->reference }}</td>
                    <td class="text-end {{ $transaction->amount < 0 ? ' text-danger' : ''}}">{{ $transaction->getFormattedAmount() }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
</div>
</body>
</html>
