<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;

class HomeController extends Controller
{
    /**
     * Handle file upload request.
     *
     * @param Request $request
     * @return View
     * @throws ValidationException
     */
    public function post(Request $request)
    {
        $this->validate($request, [
            'csv' => 'required|file|mimes:csv,txt'
        ]);

        $file = $request->file('csv');
        $transactions = $this->readFile($file);

        //sort by date
        $transactions->sort(function ($a, $b) {
            return $a->date <=> $b->date;
        });

        return view('home', compact('transactions'));
    }

    /**
     * Read csv file and return a collection of Transaction objects.
     *
     * @param UploadedFile $file
     * @return Collection collection of transactions
     */
    private function readFile(UploadedFile $file): Collection
    {
        $handle = fopen($file, 'r');
        $transactions = collect([]);
        $isFirstLine = true;
        while (($line = fgetcsv($handle)) !== false) {
            // ignore header
            if ($isFirstLine) {
                $isFirstLine = false;
                continue;
            }
            $transactions->add(new Transaction(...$line));
        }
        fclose($handle);

        return $transactions;
    }
}
