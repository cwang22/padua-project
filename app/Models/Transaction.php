<?php


namespace App\Models;


use DateTime;

class Transaction
{
    public $date;
    public $transactionCode;
    public $customerNumber;
    public $reference;
    public $amount;

    const VALID_CHARS = ['2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C',
        'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T',
        'U', 'V', 'W', 'X', 'Y', 'Z'];

    const DATE_FORMAT = 'Y-m-d g:ia';

    /**
     * Transaction constructor.
     * @param $date
     * @param $transactionCode
     * @param $customerNumber
     * @param $reference
     * @param $amount
     */
    public function __construct($date, $transactionCode, $customerNumber, $reference, $amount)
    {
        $this->date = DateTime::createFromFormat(self::DATE_FORMAT, $date);
        $this->transactionCode = $transactionCode;
        $this->customerNumber = intval($customerNumber);
        $this->reference = $reference;
        $this->amount = intval($amount);
    }

    public function getFormattedDate(): string
    {
        return $this->date->format(self::DATE_FORMAT);
    }

    public function getFormattedAmount(): string
    {
        $sign = $this->amount >= 0 ? '' : '-';
        $num = number_format(abs($this->amount / 100), 2);
        return $sign . '$' . $num;
    }

    public function isValid(): bool
    {
        $key = $this->transactionCode;

        if (strlen($key) !== 10) {
            return false;
        }

        $checkDigit = $this->generateCheckCharacter(substr(strtoupper($key), 0, 9));

        return $key[9] === $checkDigit;
    }

    private function generateCheckCharacter(string $input): string
    {
        $factor = 2;
        $sum = 0;
        $n = sizeof(self::VALID_CHARS);
        for ($i = strlen($input) - 1; $i >= 0; $i--) {
            $codePoint = array_search($input[$i], self::VALID_CHARS);
            $addend = $factor * $codePoint;
            $factor = ($factor === 2) ? 1 : 2;
            $addend = intdiv($addend, $n) + ($addend % $n);
            $sum += $addend;
        }

        $remainder = $sum % $n;
        $checkCodePoint = ($n - $remainder) % $n;
        return self::VALID_CHARS[$checkCodePoint];
    }

}
