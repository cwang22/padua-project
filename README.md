## Explanation 
I've chosen to use Laravel I'm familiar with it, and I used features to speed up my development speed:
- route
- request handling
- template
- collection


## Files

Only the following files are created/modified

    ├── app
    |  ├── Http
    |  |  └── Controllers
    |  |  |  └── HomeController.php 
    |  ├── Models
    |  |  └── Transaction.php
    |  └── views
    |     └── home.blade.php
    └─ routes
       └── web.php 

## Run
    git clone git@gitlab.com:cwang22/padua-project.git
    cd padua-project
    composer install
    cp .env.example .env
    php artisan key:generate
    php artisan serve
